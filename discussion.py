# [section] Input

# input is same as prompt in JS
username = input("please enter your name:\n")
print(f"Hi {username}! Welcome to python!")

num1 = int(input("please enter first number "))
num2 = int(input("please enter second number "))
print(f"the sum of num1 and num2 is {num1 + num2}")

# [Section] If-Else statement

test_num = 75
if test_num >= 60 :
	print("test passed")
else :
	print("test failed")

# if-else chains
test_num2 = int(input("please input a number\n"))
if test_num2 > 0 :
	print("the number is positive")
elif test_num2 == 0 :
	print("the number is zero")
else :
	print("the number is negative")

test_num3 = int(input("please input a number\n"))
if test_num3 % 3 == 0 and test_num3 % 5 == 0:
	print("the number is divisible by 3 and 5")
elif test_num3 % 3 == 0:
	print("the number is divisible by 3")
elif test_num3 % 5 == 0:
	print("the number is divisible by 5")
else:
	print("the number is not divisible by 3 or 5")

# [section] loops
# while loop
i = 1
while i <= 5:
	print(f"current value: {i}")
	i += 1

# for loop
fruits = [ "apple", "orange", "banana" ]
for indiv_fruit in fruits :
	print(indiv_fruit)

# range() method allows us to iterate through values
range_value = range(6)
print(range_value)
for x in range(6):
	print(f"Current Value: {x}")
# range(stop) :start from 0 to 5

for x in range(1, 10):
	print(f"Current Value: {x}")
# range(start, stop) :start from 1 to 9

for x in range(1, 10, 2):
	print(f"Current Value: {x}")
# range(start, stop, step) :step value 2

# [section] break and continue

#break statement
j = 1
while j < 6:
	print(j)
	if j == 3:
		break
	j+=1

# continue statement used to stop the current iteration and continue to the next iteration
# takes the control back to the top of the iteration

k = 1
while k < 6 :
	k += 1
	if k == 3 :
		continue
	print(k)

'''
is not an infinite loop but rather the continue statement
is trying to find the "top" of the loop which is the "k +=1" that is skipped
after the if statement

k = 1
while k < 6:
	k += 1
	if k == 3:
		continue
	print(k)

'''

















