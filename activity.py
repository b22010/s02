#1
year = int(input("Enter Year: "))

if year % 4 == 0 and year % 100 != 0:
    print(f"{year} is a leap year")
elif year % 100 == 0:
    print(f"{year} is not a leap year")
elif year % 400 ==0:
    print(f"{year} is a leap year")
else:
    print(f"{year} is not a leap year")

#2
rows = int(input("Please Enter Rows:\n"))
columns = int(input("Please Enter Columns:\n"))

print("asterisk grid") 
for i in range(rows):
    for j in range(columns):
        print('*', end = '  ')
    print()
